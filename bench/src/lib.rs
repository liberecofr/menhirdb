#![feature(test)]
extern crate kv;
extern crate menhirkv;
extern crate serde;
extern crate test;

#[cfg(test)]
mod tests {
    use super::*;
    use kv;
    use kv::Codec;
    use menhirkv;
    use menhirkv::Store;
    use serde::{Deserialize, Serialize};
    use tempfile::TempDir;
    use test::Bencher;

    const CAPACITY_MAX: usize = 1_000_000;
    const BLOB_SIZE: usize = 10_000;

    #[derive(Serialize, Deserialize, PartialEq, Clone)]
    struct Blob {
        bytes: Vec<u8>,
    }

    impl Blob {
        fn new() -> Self {
            Blob {
                bytes: [42; BLOB_SIZE].to_vec(),
            }
        }
    }

    #[bench]
    fn bench_menhirkv_blob_max(b: &mut Bencher) {
        let store: Store<isize, Blob> = Store::open_temporary(CAPACITY_MAX).unwrap();
        let blob = Blob::new();
        let mut dummy: u8 = 0;
        let mut i: isize = 0;

        b.iter(|| {
            store.put(&i, &blob).unwrap();
            let b = store.get(&i).unwrap().unwrap();
            dummy = b.bytes[0];
            i += 1;
        });

        assert_eq!(42, dummy);
    }

    #[bench]
    fn bench_menhirkv_blob_1k(b: &mut Bencher) {
        let store: Store<isize, Blob> = Store::open_temporary(1_000).unwrap();
        let blob = Blob::new();
        let mut dummy: u8 = 0;
        let mut i: isize = 0;

        b.iter(|| {
            store.put(&i, &blob).unwrap();
            let b = store.get(&i).unwrap().unwrap();
            dummy = b.bytes[0];
            i += 1;
        });

        let len = store.len().unwrap();
        let db_len = store.db_len().unwrap();
        println!("len: {}, db_len: {}", len, db_len);

        if i > 5_000 {
            assert!(
                len < i as usize,
                "there should have been some dropped entries"
            );
        }

        if i > 20_000 {
            assert!(
                db_len < i as usize,
                "there should have been some compactions"
            );
        }

        assert_eq!(42, dummy);
    }

    #[bench]
    fn bench_menhirkv_bool_max(b: &mut Bencher) {
        let store: Store<isize, bool> = Store::open_temporary(CAPACITY_MAX).unwrap();
        let mut dummy = false;
        let mut i: isize = 0;

        b.iter(|| {
            store.put(&i, &true).unwrap();
            dummy = store.get(&i).unwrap().unwrap();
            i += 1;
        });

        assert!(dummy);
    }

    #[bench]
    fn bench_menhirkv_bool_1k(b: &mut Bencher) {
        let store: Store<isize, bool> = Store::open_temporary(1_000).unwrap();
        let mut dummy = false;
        let mut i: isize = 0;

        b.iter(|| {
            store.put(&i, &true).unwrap();
            dummy = store.get(&i).unwrap().unwrap();
            i += 1;
        });

        println!(
            "len: {}, db_len: {}",
            store.len().unwrap(),
            store.db_len().unwrap()
        );

        assert!(dummy);
    }

    #[bench]
    fn bench_extern_crate_kv_blob(b: &mut Bencher) {
        let tmp_dir = TempDir::new().unwrap();
        let tmp_path = tmp_dir.path().join("tmp.db");
        let cfg = kv::Config::new(&tmp_path);
        let store = kv::Store::new(cfg).unwrap();
        let bucket = store.bucket::<String, kv::Bincode<Blob>>(None).unwrap();
        let blob = Blob::new();
        let mut dummy: u8 = 0;
        let mut i: isize = 0;

        b.iter(|| {
            let k = format!("{}", i);
            // Bench is unfair here as we clone() the blob, OTOH I am
            // not seeing a way to not do this, due to traits details,
            // doing a Bincode<&Blob> is tricky.
            bucket.set(&k, &kv::Bincode(blob.clone())).unwrap();
            let b = bucket.get(&k).unwrap().unwrap().into_inner();
            dummy = b.bytes[0];
            i += 1;
        });

        assert_eq!(42, dummy);
    }

    #[bench]
    fn bench_extern_crate_kv_bool(b: &mut Bencher) {
        let tmp_dir = TempDir::new().unwrap();
        let tmp_path = tmp_dir.path().join("tmp.db");
        let cfg = kv::Config::new(&tmp_path);
        let store = kv::Store::new(cfg).unwrap();
        let bucket = store.bucket::<String, kv::Bincode<bool>>(None).unwrap();
        let mut dummy = false;
        let mut i: isize = 0;

        b.iter(|| {
            let k = format!("{}", i);
            bucket.set(&k, &kv::Bincode(true)).unwrap();
            dummy = bucket.get(&k).unwrap().unwrap().into_inner();
            i += 1;
        });

        assert!(dummy);
    }
}
