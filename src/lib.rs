// Copyright (C) 2024 Christian Mauduit <ufoot@ufoot.org>

//! [MenhirKV](https://gitlab.com/liberecofr/menhirkv) is yet another local KV store based on [RocksDB](https://rocksdb.org)
//!
//! ![MenhirKV icon](https://gitlab.com/liberecofr/menhirkv/raw/main/menhirkv.png)
//!
//! # Examples
//!
//! Simple put/get:
//!
//! ```
//! use menhirkv::Store;
//!
//! let store: Store<String, usize> = Store::open_temporary(100).unwrap();
//! let key = String::from("the-answer");
//! store.put(&key, &42).unwrap();
//! assert_eq!(Some(42), store.get(&key).unwrap());
//! ```
//!
//! Multiple threads:
//!
//! ```
//! use menhirkv::Store;
//! use std::thread;
//!
//! // No need to declare `mut`.
//! let store: Store<usize, usize> = Store::open_temporary(10_000).unwrap();
//!
//! let s = store.clone(); // clone() only makes a new ref (Arc)
//! let handle1 = thread::spawn(move || {
//!     for i in 0..1_000 {
//!         let j: usize = i * 2;
//!         s.put(&j, &j).unwrap();
//!     }
//! });
//!
//! let s = store.clone(); // clone() only makes a new ref (Arc)
//! let handle2 = thread::spawn(move || {
//!     for i in 0..1_000 {
//!         let j: usize = i * 2 + 1;
//!         s.put(&j, &j).unwrap();
//!     }
//! });
//!
//! handle1.join().unwrap();
//! handle2.join().unwrap();
//!
//! assert_eq!(Some(1_998), store.get(&1_998).unwrap());
//! assert_eq!(Some(1_999), store.get(&1_999).unwrap());
//! ```
//!
//! Custom type:
//!
//! ```
//! use menhirkv::Store;
//! use serde::{Serialize, Deserialize};
//!
//! #[derive(Serialize, Deserialize)]
//! struct Id {
//!     no: usize,
//!     code: u8,
//! }
//!
//! #[derive(Serialize, Deserialize)]
//! struct Person {
//!     name: String,
//!     age: usize,
//! }
//!
//! let store: Store<Id, Person> = Store::open_temporary(100).unwrap();
//! let k = Id{no: 123, code: 99};
//! let v = Person{name: String::from("Me"), age: 47};
//!
//! store.put(&k, &v).expect("put should work");
//! let who = store.get(&k).expect("get should work").unwrap();
//!
//! assert_eq!(47, who.age);
//! ```
//!
//! Error handling:
//!
//! ```
//! use menhirkv::{Store, Result};
//! use std::path::Path;
//!
//! fn do_stuff() -> Result<bool> {
//!     // no need to `.unwrap()`, bubble up errors with `?;`
//!     let store: Store<usize, bool> = Store::open_with_path("/tmp/maybe-yes-maybe-not.db", 100)?;
//!     let answer = store.get(&42)?;
//!     match answer {
//!         Some(a) => Ok(a),
//!         None => Ok(false),
//!     }
//! }
//!
//! match do_stuff() {
//!     Ok(a) => println!("answer: {}", a),
//!     Err(e) => println!("that did not went well: {}", e),
//! }
//! ```
//!
//! Size is capped:
//!
//! ```
//! use menhirkv::Store;
//!
//! let store: Store<usize, usize> = Store::open_temporary(100).unwrap();
//! for i in 0..10_000 {
//!     store.put(&i, &i).unwrap();
//! }
//! let len = store.len().unwrap(); // do not call this in prod, it is slow
//! println!("len: {}", len);
//! assert!(len >= 100);
//! assert!(len < 1_000);
//! assert_eq!(Some(9_999), store.get(&9_999).unwrap());
//! ```
//!
//! Multiple column families (AKA namespaces):
//!
//! ```
//! use menhirkv::Store;
//!
//! let store: Store<usize, usize> = Store::open_cf_temporary(&["hip", "hop"], 100).unwrap();
//! store.put(&1, &100).unwrap();
//! let store_hip = store.cf("hip").unwrap();
//! store_hip.put(&1, &10).unwrap();
//! store_hip.put(&2, &20).unwrap();
//! let store_hop = store_hip.cf("hop").unwrap();
//! store_hop.put(&1, &1).unwrap();
//! store_hop.put(&2, &2).unwrap();
//! store_hop.put(&3, &3).unwrap();
//!
//! assert_eq!(Some(100), store.get(&1).unwrap());
//! assert_eq!(None, store.get(&2).unwrap());
//! assert_eq!(None, store.get(&3).unwrap());
//!
//! assert_eq!(Some(10), store_hip.get(&1).unwrap());
//! assert_eq!(Some(20), store_hip.get(&2).unwrap());
//! assert_eq!(None, store_hip.get(&3).unwrap());
//!
//! assert_eq!(Some(1), store_hop.get(&1).unwrap());
//! assert_eq!(Some(2), store_hop.get(&2).unwrap());
//! assert_eq!(Some(3), store_hop.get(&3).unwrap());
//! ```

mod error;
mod iter;
mod serial;
mod store;

pub use error::*;
pub use iter::*;
pub use store::*;

pub mod rocksdb {
    pub use rocksdb::{Options, DB};
}
